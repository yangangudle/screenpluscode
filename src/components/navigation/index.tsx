import React from 'react';
import Navbar from 'react-bootstrap/Navbar';
import { Code } from 'react-feather';

const Navigation = () => (
    <>
        <Navbar className="code-header" >
            <Navbar.Brand href="#home">
            <Code />
            {' '}
            Screen + Code
            </Navbar.Brand>
        </Navbar>
    </>
)

export default Navigation;