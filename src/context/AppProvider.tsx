import React, { createContext, useEffect, useState } from 'react';
import axios from 'axios';

type AppContextProps = {
    url: string, 
    data: any,
    handleSetUrl: (url: string) => any,
    getFile: (url: string) => any, 
    fileContent: any, 
    downloadUrl: string | null,
    setDownloadUrl: any,
    repo: any,
    clearError: () => any, 
    error: any,
    channel: any
}

export const AppContext = createContext<Partial<AppContextProps>>({});

const HTTP_SUCCESS = 200;

const AppContextProvider = ({ children }: any) => {
    const [ url, setUrl ]: [ any, any ] = useState(localStorage.getItem('url'));
    const [ baseUrl, setBaseUrl ]: [any, any ] = useState(localStorage.getItem('baseUrl'));
    const [ data, setData ]: [ any, any ] = useState(null);
    const [ repo, setRepo ]: [ any, any ] = useState(null);
    const [ fileContent, setFileContent ] : [ any, any ] = useState(null);
    const [ downloadUrl, setDownloadUrl ] : [ any, any ] = useState(null);
    const [ error, setError ] : [ any, any ] = useState(null);
    const [ channel, setChannel ]: [ any, any ] = useState('')
    
    const clearError = () => {
        setError(null);
    }

    // TODO: Make this mad comprehensive for different services
    const handleSetUrl = (url: string) => {
        const api_url = url.replace('github.com', 'api.github.com/repos');
        setBaseUrl(api_url);
        setUrl(`${api_url}/contents`);
        localStorage.setItem('url', `${api_url}/contents`);
        localStorage.setItem('baseUrl',  api_url);        
    }

    const getRepoInformation = (url: string) => {
        axios.get(baseUrl).then((result: any) => {
            (result.status === HTTP_SUCCESS ) && setRepo(result.data);
        }).catch(() => setError('Failed to get repo information'))
    }

    const getFile = (fileUrl: string) => {
        axios.get(fileUrl).then((result) => {
            (result.status === HTTP_SUCCESS) && setFileContent(result.data)
        }).catch(() => setError('Failed to get file.'))
    }

    useEffect(() => {
        const current_url = new URL(window.location.href);
        const q = current_url.searchParams.get("q");
        
        if(q) {
            handleSetUrl(q);
        }
        
        if(!url) {
            return;
        }
        
        getRepoInformation(baseUrl);
  
        axios.get(url).then((result: any) => { 
            (result.status === HTTP_SUCCESS) ? setData(result.data) : setData([]); 
        }).catch(() => setError('Failed to get URL'))

    }, [ url, baseUrl ]);

    return (
        <AppContext.Provider
            value={{
                url, 
                handleSetUrl,
                data,
                getFile,
                fileContent,
                downloadUrl,
                setDownloadUrl,
                repo, 
                clearError,
                error,
                channel
            }}
        >
            {children}
        </AppContext.Provider>
    )

}

export default AppContextProvider;