import React, { useState } from 'react';
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';
import { Command } from 'react-feather';

const CodeQualityButton = () => {
    const [ isOpen, setIsOpen ] = useState(false);
    return (
        <>
            <Modal show={isOpen} onHide={() => setIsOpen(!isOpen)}>
                <Modal.Header closeButton>
                    <Modal.Title>Code Quality</Modal.Title>
                </Modal.Header>
                <Modal.Body>Sorry <code>Code Quality Check</code> is currently unavailable</Modal.Body>
            </Modal>
            <Button 
                variant="dark" 
                className="edit-button"
                onClick={() => setIsOpen(true)}
            >
                <Command /> Code Quality </Button>   
        </>
    )
}

export default CodeQualityButton;