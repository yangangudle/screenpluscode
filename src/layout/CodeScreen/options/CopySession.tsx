import React, { useState } from 'react';
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';
import { Share2 } from 'react-feather';

const CopySession = () => {
    const [ isOpen, setIsOpen ] = useState(false);
    const current_url = new URL(window.location.href);
    return (
        <>
          <Modal show={isOpen} onHide={() => setIsOpen(!isOpen)}>
                <Modal.Header closeButton>
                    <Modal.Title>Invite Url</Modal.Title>
                </Modal.Header>
<Modal.Body>Url: <a target="_blank" href={current_url.toString()}>{current_url.toString()}</a></Modal.Body>
            </Modal>
            <Button 
                variant="dark" 
                className="edit-button"
                onClick={() => setIsOpen(true)}
            >
                <Share2 />Share</Button>   
        </>
    )
};

export default CopySession;
