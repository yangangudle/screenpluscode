import React, { useState, ReactElement } from 'react';
import ListGroup from 'react-bootstrap/ListGroup';
import { Folder } from 'react-feather';

interface IDirectory {
    children: any,
    name: string, 
    renderFileList: (data: any, projectName: string, sub: boolean) => ReactElement
}

const Directory = ({ name, children, renderFileList }: IDirectory) => {
    const [ isExpaned, setIsExpaned ] = useState(false);

    return (
        <>
            <ListGroup.Item onClick={() => setIsExpaned(!isExpaned)}><Folder /> {name}</ListGroup.Item>
            {isExpaned ? renderFileList(children, '', true): <span></span>}
        </>
    )
}

export default Directory;