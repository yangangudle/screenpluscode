import React, { useState, useContext } from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import Card from 'react-bootstrap/Card'
import { Play, DownloadCloud } from 'react-feather';
import * as uuid from 'uuid';
import { CodeEditor } from '../../components/editor';
import { FileList } from '../../components/editor';
import { CodeQualityButton, CodeRunButton, CopySession } from './options';

import './style.css';
import { AppContext } from '../../context/AppProvider';

const ENTER_KEY_CODE = 13;

const CodeScreen = () => {
    const appContext = useContext(AppContext);
    const [ url, setUrl ] = useState('');

    return(
        <Container fluid={true}>
            <Row>
                <Col className="panel file" xs="2"> 
                    <FileList />
                    <Card>
                        
                        <Card.Body>
                            <Card.Title>Your Ad Here</Card.Title>
                            <Card.Text>
                                <code>Your super cool ad could appear here.</code>
                            </Card.Text>
                        </Card.Body>
                    </Card>
                </Col>
                <Col className="panel code" xs="10"> 
                    <Col className="control-panel">
                        <Form inline> 
                            <Form.Control 
                                value={url}
                                onChange={(e: any) => setUrl(e.target.value) }
                                onKeyDown={(e: any) => {
                                    if(e.keyCode === ENTER_KEY_CODE) {
                                        e.preventDefault();
                                        appContext.handleSetUrl && appContext.handleSetUrl(url);
                                    }   
                                }}
                                type="url" 
                                placeholder="Paste Github URL" />
                            <Button 
                                onClick={() => { 
                                    appContext.handleSetUrl && appContext.handleSetUrl(url)
                                    const current_url = new URL(window.location.href);
                                    const q = url || current_url.searchParams.get("q") || url;
                                    window.location.href = `http://localhost:3000?q=${q}&channel=${uuid.v1()}`
                                }}
                                variant="dark" 
                                className="edit-button"><Play /> Pull Code</Button> 
                            <Button variant="dark" className="edit-button" onClick={
                                () => { 
                                    if(appContext.downloadUrl) {
                                        window.open(appContext.downloadUrl);    
                                    }
                                }
                            }><DownloadCloud /> Download</Button> 
                            <CodeQualityButton />
                            <CodeRunButton />
                            <CopySession />
                        </Form>
                    </Col>
                    <CodeEditor />
                </Col>
            </Row>
        </Container>
    )
};

export default CodeScreen;
