import React from 'react';
import './App.css';

import AppContext from './context/AppProvider';
import Navigation from './components/navigation';
import CodeScreen from './layout/CodeScreen';
import Error from './components/errors';
import Meeting from './components/meetings';

const App = () => {

  return (
    <AppContext>
      <Navigation />
      <CodeScreen />
      <Error />
      <Meeting />
    </AppContext>
  );
}

export default App;
